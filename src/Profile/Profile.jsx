import { useNavigate } from "react-router-dom";
import { ClearHistory, translationAdd } from "../api/translations";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { useUser } from "../context/UserContext";
import WithAuth from "../hoc/withAuth";
import './Profile.css';


const Profile = () => {
    const Navigate = useNavigate()
    const  { user, setUser } = useUser();

    let Array = user.translations;
    while (Array.length > 10) {             // If more than 10 items in array, delete the first one until sufficient length
        Array.shift();
    }
    console.log(Array)

    const DeleteHistory = async () => {
        const [clearError, clearResult] = await ClearHistory(user.id) // Might not work properly?
        setUser({
            ...user,
            translations: []
        })
    }

    function NavigateToTranslate() {
        Navigate("/translate");
    }

    return (
        <div>
        <h1>Profile</h1>
        <button onClick={NavigateToTranslate}>Translate</button>
        <p> Welcome {user.username}</p>
        
        <p>History</p>
        <table class="center">
            <th> Your previous searches</th>
        {Array.map( (array)  =>  < tr>{array}</tr>)}
        </table>
        <button onClick={DeleteHistory}>Delete history</button>
        </div>
    )
}

export default WithAuth(Profile);