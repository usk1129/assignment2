import { createHeaders } from "./index"
const ApiUrl = process.env.REACT_APP_API_URL

export const CheckForUser = async (username) => {
    try{
        const response = await fetch(`${ApiUrl}?username=${username}`)
        if(!response.ok){
            throw new Error('Could not complete request')
        }
        const data = await response.json()
        return [null, data]

    }
    catch (error) {
        return [ error.message, []]

    }

}


export const CreateUser = async (username) => {
    try{
        const response =  await fetch(ApiUrl,{
            method: 'POST',
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations: []
            })
        })
        if(!response.ok){
            throw new Error('Could not create user with username ' + username)
        }
        const data = await response.json()
        return [null, data]

    }
    catch (error) {
        return [ error.message, []]
    }

}


export const GetUser = async (username) => {
    try{
        const response =  await fetch(ApiUrl,{
            method: 'GET',
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations: []
            })
        })
        if(!response.ok){
            throw new Error('Could not get username ' + username)
        }
        const data = await response.json()
        return [null, data]

    }
    catch (error) {
        return [ error.message, []]
    }

}


export const LoginUser = async (username) => { 
    
    const [ checkError,user] = await CheckForUser(username)

    if(checkError !== null){
        return [checkError,null]
    }

    if(user.length > 0){
        //user does not exist
        return [ null, user.pop() ]
    }
    return await CreateUser(username)


}