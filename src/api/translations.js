import { createHeaders } from "./index"
const ApiUrl = process.env.REACT_APP_API_URL

export const TranslationAdd = async (user,translation) => {
    try {
        const response = await fetch(`${ApiUrl}?user.id=${user}`, {
            method: "POST",
            headers: createHeaders(),
            body: JSON.stringify({ 
                username: user.username,
                translations: [...user.translations, translation] 
            })
            
        })
        console.log(user)
        if(!response.ok){
            throw new Error('Could not update the order ')
        }
        const result = await response.json()
        return [null, result]
    }
    catch (error) {
        return [ error.message, null]
    }

}    

export const ClearHistory = async (user,translation) => {
    try {
        const response = await fetch(`${ApiUrl}?user.id=${user}`, {
            method: "PATCH",
            headers: createHeaders(),
            body: JSON.stringify({ 
                username: user.username,
                translations: [...user.translations, translation] 
            })
            
        })
        console.log(user)
        if(!response.ok){
            throw new Error('Could not update the order ')
        }
        const result = await response.json()
        return [null, result]
    }
    catch (error) {
        return [ error.message, null]
    }

}    