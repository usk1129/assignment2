import './App.css';
import { Router, BrowserRouter, Routes, Route } from 'react-router-dom';
import Login from './Login/Login';
import Profile from './Profile/Profile';
import Translate from './Translate/Translate';
import TranslateForm from './Translate/TranslateForm';


function App() {

  return (
    <BrowserRouter>
    <div className="App">
      <Routes>
        <Route path="/" element= { <Login />}/>
        <Route path="/Profile" element= { <Profile />}/>
        <Route path="/Translate" element= { <Translate />}/>
      </Routes>
    </div>
    </BrowserRouter>
    
  );
}

export default App;
