import { useForm } from 'react-hook-form'
import { LoginUser } from '../api/user';
import { useState,useEffect } from "react";
import { storageSave } from '../utils/storage';
import { useNavigate } from 'react-router-dom'
import { useUser } from '../context/UserContext'
import { STORAGE_KEY_USER } from '../const/storageKeys';


const usernameConfig = {
    required: true,
    minLength: 2
}


const LoginForm = () => {
    // Hooks
    const {register,handleSubmit,formState: {errors}} = useForm();
    const { user,setUser } = useUser();

    const navigate = useNavigate();
    //Local State
    const [loading , SetLoading ] = useState(false)
    const [apiError, setApiError] = useState(null)

    //Side Effects
    useEffect(() => {
        console.log('User has changed',user)
        if (user !== null) {
            navigate("translate");
        }


        

    },[user, navigate]) //Empty Deps - Only run once

    //Event Handlers
    const onSubmit = async (username) => {
        SetLoading(true);
        const [ error,userResponse ] = await LoginUser(username.username)
        if(error !== null){
            setApiError(error);
        }
        if(userResponse !== null){ //check if user is already registered
            storageSave(STORAGE_KEY_USER, userResponse)
            setUser(userResponse);
        }

        SetLoading(false);

    }

    // Render Functions
    const errorMessage = (() => {
        if(!errors.username){
            return null
        }

        if(errors.username.type === 'required'){
            return <span>Username is required</span>

        }

        if(errors.username.type === 'minLength'){
            return <span>Username is too short(min 2)</span>
        }

    })()
    return(
        <>
            <h2>Hello ! :)</h2>
            <form onSubmit={ handleSubmit(onSubmit) }>
                <fieldset>
                    <label htmlFor="username">Username: </label>
                    <input type="text" placeholder="What is your name?"
                    { ...register("username",usernameConfig) }/>
                    { errorMessage }
                </fieldset>
                <button type="submit" disabled={ loading }>Log in</button>
                {loading && <p>Loggin in...</p>}
                {apiError && <p>{ apiError }</p>}
            </form>
        </>
    )
}



export default LoginForm