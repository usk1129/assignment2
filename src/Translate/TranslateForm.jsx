import { useState } from "react";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { TranslationAdd } from "../api/translations";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { useUser } from '../context/UserContext'
import { storageSave } from "../utils/storage";

const TranslateForm = () => {
    const navigate = useNavigate();
    const [ImageArray, SetImageArray] = useState([])
   
    const { register, handleSubmit, formState: { errors } } = useForm()
    
    const { user, setUser } = useUser();
    const onSubmit = async (data) => {
        ImageArray.length = 0
        let TranslateSplit = data.ToTranslate.split("")
        TranslateSplit = TranslateSplit.map(x => "img/"+x+".png")
        console.log(user.username)
        const [error, updatedUser] = await TranslationAdd(user, data.ToTranslate)
        console.log(user)
        SetImageArray([...ImageArray, ...TranslateSplit])
          storageSave(STORAGE_KEY_USER, updatedUser)
        setUser(updatedUser)
        console.log(data.ToTranslate)
        
        

    }

    function NavigateToProfile() {
        navigate("/profile");
    }

    function Logout() {
        console.log("Button clicked")
        window.localStorage.clear()
        window.location.reload();
    }

    return (
        <div>
            <button onClick={NavigateToProfile}>Profile</button>
        <form onSubmit={ handleSubmit(onSubmit) }>
            <fieldset>
                <label htmlFor="Translate-Letters">What do you want to translate?</label> <br/>
                <input type="text" {...register('ToTranslate')}/>
            </fieldset>
            <button type="submit">Translate!</button>
        </form>
        { ImageArray.map( (image)  =>  < img src={image} alt="  "  />  )}
        <br></br>
        <button onClick={Logout}>Logout</button>
        
        </div>
    )

}


export default TranslateForm