## Assignment 2 Create a Sign Language Translator using React

Lost in Translation
Build an online sign language translator as a Single Page Application using the React framework.


## Table of Contents

- [Clone](#clone)
- [Install](#install)
- [Usage](#usage)
- [API](#api)
- [Start](#start)
- [Contributors](#contributors)


## Clone

open your termninal or cmdline

Clone the repository with:
```
$git clone https://gitlab.com/usk1129/assignment2 

$cd assignment2
```


## Install
```
$npm install react react-dom
$npm i react-hook-form
$npm install react-router-dom
```

## Usage

The goal of this react application is to create user or login with existing user from Heroku API. After that the use will navigate to the translate page where the user automatically be redirected to the Translation page and may enter text to trigger the translation and will get the The Sign language characters image. There is profile page for the users as well with history on with the last 10 translations.

## local environment variable configuration

create a new file .env
and add these lines in it:
```
REACT_APP_API_KEY=(INSERT KEY PROVIDED HERE)
REACT_APP_API_URL=(INSERT API URL PROVIDED HERE)
```

## START

Runs the app in the development mode.
```
$npm start
```
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.


## API
https://usk-noroff-api.herokuapp.com/translations

## Contributors

Usko - @usk1129 

Erik - @ermo1222





